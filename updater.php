<?
if(IsModuleInstalled('hardkod.turboyandex'))
{
	if (is_dir(dirname(__FILE__).'/install/js'))
		$updater->CopyFiles("install/js", $_SERVER["DOCUMENT_ROOT"]."/bitrix/js/hardkod.turboyandex");
	
	if (is_dir(dirname(__FILE__).'/install/css'))
		$updater->CopyFiles("install/css", $_SERVER["DOCUMENT_ROOT"]."/bitrix/css/hardkod.turboyandex");
	
	if (is_dir(dirname(__FILE__).'/install/handler'))
		$updater->CopyFiles("install/handler", $_SERVER["DOCUMENT_ROOT"]."/bitrix/tools/hardkod.turboyandex");
	
}
?>